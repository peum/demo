'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MusicplayerSchema extends Schema {
  up () {
    this.create('musicplayers', (table) => {
      table.increments()
      table.string('title')
      table.string('group')
      table.string('album')
      table.string('link')
      table.string('user_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('musicplayers')
  }
}

module.exports = MusicplayerSchema
