describe('Test MusicPlayer app - http://localhost:3333', function () {

  before(function () {

  })

  it('Obtenir la pàgina principal', function () {
    cy.visit('http://localhost:3333', {
      timeout: 50000, // increase total time for the visit to resolve
      onBeforeLoad(contentWindow) {
        // contentWindow is the remote page's window object
        expect(typeof contentWindow === 'object').to.be.true
      },
      onLoad(contentWindow) {
        // contentWindow is the remote page's window object
        expect(typeof contentWindow === 'object').to.be.true
      },
    })
    
    
  })

  it("Try to load the main page", function () {
    cy.get('#logo')
    .should('exist')
})
});