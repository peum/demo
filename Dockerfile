FROM ubuntu:16.04

#Instal a mariadb server
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install mysql-common mysql-client -y
RUN apt-get -y install curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_10.x  | bash -
RUN apt-get -y install nodejs

# Install Cypress dependencies (separate commands to avoid time outs)
RUN apt-get install -y \
    libgtk2.0-0
RUN apt-get install -y \
    libnotify-dev
RUN apt-get install -y \
    libgconf-2-4 \
    libnss3 \
    libxss1
RUN apt-get install -y \
    libasound2 \
    xvfb

RUN mkdir -p /opt/app
WORKDIR /opt/app

#Copy all the code
COPY . ./

RUN chmod +x docker/*
RUN ln -s /opt/app/docker/test /usr/sbin/test
RUN ln -s /opt/app/docker/runapp /usr/sbin/runapp
RUN ln -s /opt/app/docker/entrypoint.sh /usr/sbin/entrypoint.sh

#Execute the app deployment
RUN npm install

#Create the testing cypress video/screenshot video output directories 
RUN mkdir -p /opt/app/cypress/screenshots
RUN mkdir -p /opt/app/cypress/videos

EXPOSE 3333

ENTRYPOINT ["/opt/app/docker/entrypoint.sh"]

CMD ["/usr/sbin/runapp"]



