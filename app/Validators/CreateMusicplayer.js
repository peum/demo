'use strict'

class CreateMusicplayer {
  get rules () {
    return {
      // validation rules
      'album':'required',
      'group':'required',
      'title':'required'
    }
  }

  get messages () {
    return {
      'required' : 'Wow now, {{field}} is requierd'
    }
  }

  async fails(error) {
    this.ctx.session.withErrors (error).flashAll();

    return this.ctx.response.redirect('back');
  }
}

module.exports = CreateMusicplayer
