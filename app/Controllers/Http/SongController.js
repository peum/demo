'use strict'

class SongController {

  /**
  * @swagger
  * /api/song/all:
  *   get:
  *     tags:
  *       - Test
  *     summary: Sample API
  *     responses:
  *       200:
  *         description: Get all the songs
  *         example:
  *           message: Hello Guess
  */
  async all({ request, response }) {
    response.send({ message: 'Hello ' })
  }

  /**
  * @swagger
  * /api/song/\:id\::
  *   get:
  *     tags:
  *       - Test
  *     summary: Sample API
  *     responses:
  *       200:
  *         description: Get  the song
  *         example:
  *           message: Hello Guess
  */
  async song({ request, response }) {
    response.send({ message: 'Hello '  })
  }

  /**
  * @swagger
  * /api/song/\:id\::
  *   post:
  *     tags:
  *       - Test
  *     summary: Sample API
  *     responses:
  *       200:
  *         description: Add  the song
  *         example:
  *           message: Hello Guess
  */
 async addsong({ request, response }) {
    response.send({ message: 'Hello '  })
  }

}

module.exports = SongController
