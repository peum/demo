'use strict'

const MusicPlayer = use ('App/Models/Musicplayer')
const MusicPlayerService  = use ('App/Services/MusicPlayer')



class MusicplayerController {

    async home ({view}) {

        //Fetch all albums

        const songs = await MusicPlayer.all();
        const meganumber = await MusicPlayerService.remove(2,5);
        
        return view.render('index', {songs: songs.toJSON(), numb: meganumber});
    }

    async userIndex ({view,auth}) {
        const songs = await auth.user.musicplayers().fetch();
        return view.render('mysongs', {songs: songs.toJSON()});

    }
    
    async create ({request, auth, response, session}) {
        const {title, album, group, link} = request.all();

        /*const song = new MusicPlayer ();
        song.album = album;
        song.title = title;
        song. grup = group;
        song.link = link;
        song.user_id = session.user_id;

        await song.save();

        return response.redirect ('/');
        */

        const posted = await auth.user.musicplayers().create ( {
            'album': album,
            'title': title,
            'group': group,
            'link': link
        });

        session.flash ({message: 'The musicplayer has been added'});
        return response.redirect ('/post-a-song');

    }

    async delete ({request,auth, response, session, params}) {
        const musicplayer = MusicPlayer.find (params.id);
        await musicplayer.delete();

        session.flash ({message: 'The musicplayer has been deleted'});
        return response.redirect ('back');
    }

    async edit ({params, view}) {
        const musicplayer = await MusicPlayer.find(params.id);
        
        return view.render ('postsong', {song: musicplayer});
    }

    async update ({request, auth, response, session, params}) {
        const musicplayer = await MusicPlayer.find (params.id);
        const {album,group, title, link} = request.all();

        musicplayer.album = album;
        musicplayer.group = group;
        musicplayer.title = title;
        musicplayer.link = link;

        await musicplayer.save();

        session.flash ({message: 'The musicplayer has been updated'});
        return response.redirect ('/post-a-song');
    }



}

module.exports = MusicplayerController
