


class MusicPlayerService {

    static async add (number1, number2) {
        return number1 + number2;
    }

    static async remove (number1, number2) {
        return number1 - number2;
    }

}

module.exports = MusicPlayerService;