'use strict'

const { test } = use('Test/Suite')('Example')
const MusicPlayerService = use ('App/Services/MusicPlayer')

test('make sure 2 + 2 is 4', async ({ assert }) => {
  assert.equal(await MusicPlayerService.add (2,2), 4)
})
