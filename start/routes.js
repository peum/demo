'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

//Route.on('/').render('index')
Route.get('/','MusicplayerController.home');

Route.on ('/signup').render('auth.signup');
Route.post ('/signup','UserController.create').validator('CreateUser');
Route.on ('/login').render('auth.login');
Route.post ('/login', 'UserController.login').validator('LoginUser');

Route.get ('/logout', async ({auth, response}) => {
    await auth.logout();
    return response.redirect ('/');
});

Route.on ('post-a-song/add').render('postsong');

Route.get ('/post-a-song','MusicplayerController.userIndex');
Route.get ('/post-a-song/edit/:id','MusicplayerController.edit');
Route.get ('/post-a-song/delete/:id','MusicplayerController.delete');
Route.post ('/post-a-song/update/:id','MusicplayerController.update').validator('CreateMusicPlayer');
Route.post ('/post-a-song','MusicplayerController.create').validator('CreateMusicPlayer');

Route.get ('/api/song/all', 'SongController.all');
Route.get ('/api/song/:id:', 'SongController.song');
Route.post ('/api/song', 'SongController.addsong');

